﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCP_IP_Client.Model
{
    public class Client
    {
        public string Username { get; set; }
        public string groupName { get; set; }
        public string ActivityRank { get; set; }
    }
}
