﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCP_IP_Client.Model;

namespace TCP_IP_Client
{
    class Program
    {
        public static Thread Sender, Reciever;

        //public static Semaphore sp = new Semaphore(1, 1);

        private static Client PersistendClient = new Client();

        private static Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private static IPEndPoint local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5000);

        private static Guid MyID = Guid.Empty;

        static void Main(string[] args)
        {
            Console.Write("Enter Your Username : ");
            PersistendClient.Username = Console.ReadLine();
            Console.Write("Enter Your Room : ");
            PersistendClient.groupName = Console.ReadLine();
            Console.Write("Enter Your Rank : ");
            PersistendClient.ActivityRank = Console.ReadLine();

            Sender = new Thread(new ThreadStart(SendLoop));
            Reciever = new Thread(new ThreadStart(ReceiveLoop));

            LoopConnect(PersistendClient);

            Sender.Start();
            Reciever.Start();

            Console.ReadLine();
        }

        private static void SendLoop()
        {
            string message = null;
            while ((message = System.Console.ReadLine()) != null)
            {
                Message msg = new Message()
                {
                    ConnectionID = MyID
                    ,
                    MessageText = message
                };
                _clientSocket.Send(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(msg)));
                MessageWritten(message);
            }
        }

        private static void MessageWritten(string message)
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
            Console.WriteLine("Me : " + message);

        }
        private static void ReceiveLoop()
        {

            while (true)
            {

                byte[] recievedBuff = new byte[1024];
                int rec = _clientSocket.Receive(recievedBuff);
                byte[] data = new byte[rec];
                Array.Copy(recievedBuff, data, rec);
                string txt = Encoding.ASCII.GetString(data);
                Message msg = JsonConvert.DeserializeObject<Message>(txt);

                if (msg.Init)
                {
                    MyID = msg.ConnectionID;
                    Console.WriteLine(msg.MessageText);
                }
                else
                {
                    Console.WriteLine(msg.MessageText);
                }


            }
        }

        private static void LoopConnect(Client C)
        {
            int attempts = 0;
            while (!_clientSocket.Connected)
            {
                try
                {
                    attempts++;
                    _clientSocket.Connect(local);

                }
                catch (SocketException)
                {
                    Console.Clear();
                    Console.WriteLine("Connection attempts : {0}", attempts.ToString());
                }
            }

            Message first = new Message()
            {
                Init = true
                ,
                ConnectionID = Guid.Empty
                ,
                MessageText = JsonConvert.SerializeObject(C)
            };


            _clientSocket.Send(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(first)));
            Console.Clear();

            Console.WriteLine("Connection Established...\n");

            Console.WriteLine("Username : {0}", PersistendClient.Username);
            Console.WriteLine("ChatRoom : {0}\n", PersistendClient.groupName);

        }
    }
}
