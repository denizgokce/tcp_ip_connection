﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP_IP_Host.Model;

namespace TCP_IP_Host.User
{
    public abstract class AbstractUser
    {
        public Guid ConnectionID { get; set; }
        public string Username { get; set; }
        public string groupName { get; set; }
        public Socket Socket { get; set; }
        public string ActivityRank { get; set; }

        public delegate void NeedToBroadCastMessageHandler(Message msg);

        public event NeedToBroadCastMessageHandler NeedToBroadCastMessage;

        public AbstractUser(Socket socket)
        {
            ConnectionID = Guid.NewGuid();
            Socket = socket;

            byte[] _buffer = new byte[1024];
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = socket,
                _buffer = _buffer
            });
        }

        public void RecieveCallback(IAsyncResult AR)
        {
            passValue pv = (passValue)AR.AsyncState;
            int recieved = pv._socket.EndReceive(AR);
            byte[] dataBuff = new byte[recieved];
            Array.Copy(pv._buffer, dataBuff, recieved);

            string text = Encoding.ASCII.GetString(dataBuff);

            Message msg = JsonConvert.DeserializeObject<Message>(text);

            NeedToBroadCastMessage?.Invoke(msg);

            pv._socket.BeginReceive(pv._buffer, 0, pv._buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = pv._socket,
                _buffer = pv._buffer
            });
        }
    }
}
