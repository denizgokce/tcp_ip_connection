﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP_IP_Host.Model;

namespace TCP_IP_Host.User
{
    public class UserFactory
    {
        public delegate void UserCreationCompletedHandler(AbstractUser _user);

        public event UserCreationCompletedHandler UserCreationCompleted;

        public void HandleFirstTimeConnectionAndCreateUser(Socket _socket)
        {
            byte[] _buffer = new byte[1024];

            _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = _socket,
                _buffer = _buffer
            });
        }

        public void RecieveCallback(IAsyncResult AR)
        {
            passValue pv = (passValue)AR.AsyncState;
            int recieved = pv._socket.EndReceive(AR);
            byte[] dataBuff = new byte[recieved];
            Array.Copy(pv._buffer, dataBuff, recieved);

            string text = Encoding.ASCII.GetString(dataBuff);

            MessageResolve(text, pv._socket);
        }

        private void MessageResolve(string text, Socket socket)
        {
            Message m = JsonConvert.DeserializeObject<Message>(text);
            AbstractUser newUser = null;
            /*factory işlem değişcek*/
            if (m.Init)
            {
                dynamic data = JObject.Parse(m.MessageText);

                string rank = data.ActivityRank;

                switch (rank)
                {
                    case "Standart":
                        {
                            newUser = new StandartUser(socket)
                            {
                                Username = data.Username,
                                groupName = data.groupName,
                                ActivityRank = data.ActivityRank
                            };
                            break;
                        }
                    case "Premium":
                        {
                            newUser = new PremiumUser(socket)
                            {
                                Username = data.Username,
                                groupName = data.groupName,
                                ActivityRank = data.ActivityRank
                            };
                            break;
                        }
                    default:
                        {
                            newUser = new StandartUser(socket)
                            {
                                Username = data.Username,
                                groupName = data.groupName,
                                ActivityRank = data.ActivityRank
                            };
                            break;
                        }
                }

                if (UserCreationCompleted != null)
                {
                    UserCreationCompleted(newUser);
                }

                Console.WriteLine(newUser.Username + " as a " + newUser.ActivityRank + " user has connected the server..");

                UserIDAsiggnment(newUser, socket);
            }
        }

        private void UserIDAsiggnment(AbstractUser user, Socket socket)
        {
            Message msg = new Message()
            {
                Init = true,
                ConnectionID = user.ConnectionID,
                MessageText = LiveUserList.Instance.GetLiveUsers().Where(x=>x.groupName == user.groupName && x.ConnectionID != user.ConnectionID).ToList().Count.ToString() + " people having chat in your room.."
            };

            string txt = JsonConvert.SerializeObject(msg);
            socket.Send(Encoding.ASCII.GetBytes(txt));
        }
    }
}
