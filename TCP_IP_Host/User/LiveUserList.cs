﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCP_IP_Host.User
{
    public sealed class LiveUserList
    {
        private static LiveUserList instance = null;
        private static readonly object padlock = new object();
        private List<AbstractUser> _users = null;
        private LiveUserList()
        {
            _users = new List<AbstractUser>();
        }

        public static LiveUserList Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new LiveUserList();
                    }
                    return instance;
                }
            }
        }
        public List<AbstractUser> GetLiveUsers()
        {
            return this._users;
        }
    }
}
