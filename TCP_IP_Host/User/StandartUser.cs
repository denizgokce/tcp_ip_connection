﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCP_IP_Host.Model;

namespace TCP_IP_Host.User
{
    public class StandartUser : AbstractUser
    {
        protected Thread Advertiser;

        public StandartUser(Socket socket)
            : base(socket)
        {
            ConnectionID = Guid.NewGuid();
            Socket = socket;

            byte[] _buffer = new byte[1024];
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = socket,
                _buffer = _buffer
            });
            Advertiser = new Thread(new ThreadStart(AdvertiseUser));
            Advertiser.Start();
        }

        public void AdvertiseUser()
        {
            while (true)
            {
                Message msg = new Message()
                {
                    ConnectionID = Guid.Empty,
                    MessageText = "There is 10% discount on everything in steak'n shake don't miss it!"
                };
                Socket.Send(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(msg)));
                Thread.Sleep(10000);
            }
        }
    }
}
