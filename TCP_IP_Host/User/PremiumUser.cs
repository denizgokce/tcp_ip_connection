﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP_IP_Host.Model;

namespace TCP_IP_Host.User
{
    public class PremiumUser : AbstractUser
    {
        public PremiumUser(Socket socket)
            : base(socket)
        {
            ConnectionID = Guid.NewGuid();
            Socket = socket;

            byte[] _buffer = new byte[1024];
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = socket,
                _buffer = _buffer
            });
        }
    }
}
