﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using TCP_IP_Host.Model;

namespace TCP_IP_Host.WebClient
{
    public class WebClientFactory
    {
        public delegate void ClientCreationCompletedHandler(WebClient _user);

        public event ClientCreationCompletedHandler ClientCreationCompleted;

        public void HandleFirstTimeConnectionAndCreateClient(Socket _socket)
        {
            byte[] _buffer = new byte[1024];

            _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = _socket,
                _buffer = _buffer
            });
        }

        protected void RecieveCallback(IAsyncResult AR)
        {
            passValue pv = (passValue)AR.AsyncState;
            int recieved = pv._socket.EndReceive(AR);
            byte[] dataBuff = new byte[recieved];
            Array.Copy(pv._buffer, dataBuff, recieved);

            string text = Encoding.ASCII.GetString(dataBuff);

            ClientResolve(text, pv._socket);

        }
        protected void ClientResolve(string text, Socket socket)
        {
            WebClient newClient = null;

            Regex regex = new Regex(@"Chrome/([0-9]{2})");
            Match match = regex.Match(text);

            if (match.Success)
            {
                newClient = new WebClient(socket)
                {
                    Browser = "Chrome"
                };
            }
            else
            {

            }

            if (ClientCreationCompleted != null)
            {
                ClientCreationCompleted(newClient);
            }


            ServerWelcome(socket);
        }
        protected void ServerWelcome(Socket socket)
        {
            socket.Send(Encoding.ASCII.GetBytes("<html><head></head><body>Welcome</body></html>"));
        }


    }
}
