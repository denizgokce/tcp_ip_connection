﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP_IP_Host.Model;

namespace TCP_IP_Host.WebClient
{
    public class WebClient
    {
        public Guid ConnectionID { get; set; }
        public Socket Socket { get; set; }
        public string Browser { get; set; }

      
        public WebClient(Socket socket)
        {
            ConnectionID = Guid.NewGuid();
            Socket = socket;

            byte[] _buffer = new byte[1024];
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = socket,
                _buffer = _buffer
            });
        }

        public void RecieveCallback(IAsyncResult AR)
        {
            passValue pv = (passValue)AR.AsyncState;
            int recieved = pv._socket.EndReceive(AR);
            byte[] dataBuff = new byte[recieved];
            Array.Copy(pv._buffer, dataBuff, recieved);

            string text = Encoding.ASCII.GetString(dataBuff);

            /*
             *
             * HTML Routing 
             * 
             */

            Socket.Send(Encoding.ASCII.GetBytes("<html><head></head><body>Merhaba</body></html>"));


            pv._socket.BeginReceive(pv._buffer, 0, pv._buffer.Length, SocketFlags.None, new AsyncCallback(RecieveCallback), new passValue()
            {
                _socket = pv._socket,
                _buffer = pv._buffer
            });
        }
    }
}
