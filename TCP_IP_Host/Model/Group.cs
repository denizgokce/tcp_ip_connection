﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP_IP_Host.User;

namespace TCP_IP_Host.Model
{
    public class Group
    {
        public string GroupName { get; set; }
        public DateTime CreationTime { get; set; }
        private static List<Group> Groups = null;
      
        public Group()
        {
            if (null == Groups)
            {
                Groups = new List<Group>();
            }
        }
       
        public void Add(string groupName)
        {
            if (!Groups.Where(x => x.GroupName == groupName).Any())
            {
                Groups.Add(new Group()
                {
                    GroupName = groupName
                    ,
                    CreationTime = DateTime.Now
                });
            }
        }
        public  void Remove(string groupName)
        {
            Groups.Remove(Groups.Where(x => x.GroupName == groupName).FirstOrDefault());
        }
        public  List<AbstractUser> GetGroupList(List<AbstractUser> Users, string groupName, Socket Socket)
        {
            return Users.Where(x => x.groupName == groupName && x.Socket != Socket).ToList();
        }
    }
}
