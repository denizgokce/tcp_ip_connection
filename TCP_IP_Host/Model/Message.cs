﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCP_IP_Host.Model
{
    public class Message
    {
        public Boolean Init { get; set; }
        public Guid ConnectionID { get; set; }
        public string MessageText { get; set; }
    }
}
