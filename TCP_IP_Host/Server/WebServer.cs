﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP_IP_Host.WebClient;

namespace TCP_IP_Host.Server
{
    public class WebServer : AbstractServer
    {
        protected WebClientFactory ClientFactory = null;
        protected List<WebClient.WebClient> _clients = null;

        public WebServer(int nPort, int nBacklog)
            : base(nPort, nBacklog)
        {
            if (ClientFactory == null)
            {
                ClientFactory = new WebClientFactory();
            }
        }

        protected override void SocketAccepted(Socket _socket)
        {
            ClientFactory.HandleFirstTimeConnectionAndCreateClient(_socket);
        }

        protected void NewClientInitiationCompleted(WebClient.WebClient _client)
        {
            _clients.Add(_client);
        }

      


    }
}
