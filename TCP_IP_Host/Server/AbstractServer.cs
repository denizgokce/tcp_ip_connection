﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCP_IP_Host.Server
{
    public abstract class AbstractServer
    {
        //abstract public void RecieveCallback(IAsyncResult AR);

        protected int Port { get; set; }
        protected int Backlog { get; set; }

        protected byte[] _buffer = new byte[1024];
        protected Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public AbstractServer(int nPort, int nBacklog)
        {
            Port = nPort;
            Backlog = nBacklog;
        }

        public int GetPort()
        {
            return Port;
        }

        public void SetupServer()
        {
            _serverSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), Port));
            _serverSocket.Listen(Backlog);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        public void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = _serverSocket.EndAccept(AR);

            SocketAccepted(socket);

            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        protected abstract void SocketAccepted(Socket _socket);

        public void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }      
       

    }
}
