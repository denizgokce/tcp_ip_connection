﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TCP_IP_Host.Server
{
    public class ServerFactory
    {
        private ServerFactory() { }

        private static List<AbstractServer> Servers = new List<AbstractServer>();

        public static List<AbstractServer> InitializeTheServers()
        {
            string path = Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).FullName).FullName.ToString();
            XDocument xdoc = XDocument.Load(path + "\\server.config");
            var xservers = xdoc.Elements("configuration").Elements("servers").Elements("server").ToList();
            foreach (XElement item in xservers)
            {
                if (item.Attribute("type").Value == "ChatServer")
                {
                    Servers.Add(new ChatServer(Convert.ToInt32(item.Attribute("port").Value), Convert.ToInt32(item.Attribute("backlog").Value)));
                }
                else
                {
                    Servers.Add(new WebServer(Convert.ToInt32(item.Attribute("port").Value), Convert.ToInt32(item.Attribute("backlog").Value)));
                }
            }
            return Servers;
        }
    }
}
