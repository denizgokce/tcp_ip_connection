﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCP_IP_Host.Model;
using TCP_IP_Host.User;

namespace TCP_IP_Host.Server
{
    public class ChatServer : AbstractServer
    {
        protected UserFactory _userFactory = null;

        protected List<AbstractUser> _users = null;

        public ChatServer(int nPort, int nBacklog)
            : base(nPort, nBacklog)
        {

            if (_users == null)
            {
                _users = LiveUserList.Instance.GetLiveUsers();
            }

            if (_userFactory == null)
            {
                _userFactory = new UserFactory();
            }

            _userFactory.UserCreationCompleted += NewUserInitiationCompleted;
        }

        protected void NewUserInitiationCompleted(AbstractUser _user)
        {
            _user.NeedToBroadCastMessage += BroadcastMessage;

            _users.Add(_user);

            NotifyUsers(_user);
        }

        protected override void SocketAccepted(Socket _socket)
        {
            _userFactory.HandleFirstTimeConnectionAndCreateUser(_socket);
        }

        private void NotifyUsers(AbstractUser NewUser)
        {
            _users.Where(x => x.groupName == NewUser.groupName && x.Username != NewUser.Username).ToList().ForEach(x =>
            {
                Message msg = new Message()
                {
                    Init = false
                    ,
                    ConnectionID = Guid.Empty
                    ,
                    MessageText = NewUser.Username + " has joined your Room.."
                };
                x.Socket.Send(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(msg)));
            });
        }

        protected void BroadcastMessage(Message msg)
        {
            AbstractUser usr = _users.Where(x => x.ConnectionID == msg.ConnectionID).FirstOrDefault();

            Console.WriteLine(usr.Username + " wrote " + "\"" + msg.MessageText + "\" in Room \"" + usr.groupName + "\"");

            _users.Where(x => x.groupName == usr.groupName && x.Username != usr.Username).ToList().ForEach(x =>
            {
                Message NewMessage = new Message()
                {
                    Init = false
                    ,
                    ConnectionID = Guid.Empty
                    ,
                    MessageText = usr.Username + " : " + msg.MessageText
                };
                byte[] _buffer = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(NewMessage));

                x.Socket.BeginSend(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(SendCallback), x.Socket);

            });
        }

        public void ShutdownServer()
        {
            _userFactory.UserCreationCompleted -= NewUserInitiationCompleted;
        }
    }
}
