﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCP_IP_Host.Server;

namespace TCP_IP_Host
{
    class Program
    {


        static void Main(string[] args)
        {

            List<AbstractServer> Servers = ServerFactory.InitializeTheServers();

            foreach (var server in Servers)
            {
                server.SetupServer();
                Console.WriteLine("A " + server.GetType().Name + " is running on 127.0.0.1:" + server.GetPort() + "...");
            }

            Console.ReadLine();
        }

    }
}
